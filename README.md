Python script that uses Aho-Corasick implemented in C++ to replace multiple strings in a file.  
A string to be replaced is a line in the "dictionnaire.txt" file.  

Link to the implementation: https://gitlab.com/gabriel.darbord/aho-corasick  
