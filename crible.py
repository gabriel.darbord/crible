import subprocess

def sanitize(s):
    "Supprime les séquences d'échappement ajoutées par le programme aho"
    s = s.replace("\\\n", "\n")
    s = s.replace("\\\"", "\"")
    s = s.replace("\\\\", "\\")
    return s

def isEscaped(s, pos):
    "Retourne vrai si le caractère s[pos] est échappé"
    if pos <= 0: # si au début du texte
        return False
    if s[pos-1] == '\\' and not isEscaped(s, pos-1):
        return True
    return False

def resToList(res):
    "Sépare chaque cible en une string dans un tableau"
    targets = []
    lastSplit = 0
    lastSep = 0

    while True:
        nextSplit = res.find('\n', lastSep)
        if nextSplit == -1: # fin du résultat
            targets.append(sanitize(res[lastSplit:]))
            break
        if not isEscaped(res, nextSplit): # retour à la ligne non-échappé
            targets.append(sanitize(res[lastSplit:nextSplit]))
            lastSplit = nextSplit + 1
        lastSep = nextSplit + 1
    return targets


def displayLine(lines, pos, target):
    "Affiche la ligne où se trouve la position"
    targetEnd = pos + len(target) # fin de cible
    lineNum = lines.count('\n', 0, pos) + 1 # numéro de ligne
    lineBeg = lines.rfind('\n', 0, pos) + 1 # début de ligne

    # fin de ligne
    lineEnd = lines.find('\n', targetEnd)
    lineEnd = (len(lines) if lineEnd == -1 else lineEnd)

    # mise en évidence de la cible
    print(f"{lineNum}:" + lines[lineBeg:pos] + "\033[31m" + lines[pos:targetEnd] + "\033[m" + lines[targetEnd:lineEnd])


# demande du fichier cible
fileTarget = input("Fichier à cribler : ")


try: # utilisation d'une sérialisation si elle existe
    with open("serialisation.jnk", 'r'): pass
    cmd = ["./aho", "-s", "-l", f"{fileTarget}"]
except FileNotFoundError:
    try: # sinon lecture depuis le fichier dictionnaire.txt
        with open("dictionnaire.txt", 'r') as f:
            first = f.read(1)
            if not first:
                print("Le fichier dictionnaire.txt est vide !")
                exit()
        cmd = ["./aho", "-s", "-f", "dictionnaire.txt", f"{fileTarget}"]
    except FileNotFoundError:
        print("Le fichier dictionnaire.txt est manquant !")
        exit()


# exécution du programme aho_corasick
result = subprocess.run(cmd,
                        stdout=subprocess.PIPE, #capture de stdout
                        check=True) #vérifie que la commande ne retourne pas d'erreur


# récupération du résultat en une string (en découpant le dernier '\n')
result = result.stdout.decode('utf-8')[:-1]


# arrêt si aucun résultat
if not result:
    print("Aucune cible trouvée !")
    exit()


# transformation de la string en tableau de cibles
targets = resToList(result)


# utilisation du tableau pour construire la liste de positions et le dictionnaire de substitutions
positions = [] # (position, "cible")
substitutions = {} # "cible" => "remplacement"
for line in targets:
    pos, word = line.split(':', 1)
    positions.append((int(pos), word))
    substitutions[word] = None


# mise en mémoire du fichier cible en une seule string
with open(fileTarget, 'rb') as f:
    lines = "".join([line.decode('utf-8') for line in f.readlines()])
    nbSub = len(substitutions)
    print(f"\n{nbSub}", ("cibles trouvées" if nbSub > 1 else "cible trouvée"), ":", [sub for sub in substitutions])


# affichage des lignes où la cible apparait
for sub in substitutions:
    print(f"\nLigne(s) contenant {repr(sub)} :")
    [displayLine(lines, pos, target) for pos, target in positions if target == sub]

    # choix de la valeur de remplacement
    substitutions[sub] = input(f"Remplacer {repr(sub)} par : ")


# itération inversée pour ne pas impacter les positions suivantes
for pos, sub in reversed(positions):
    lines = lines[:pos] + substitutions[sub] + lines[pos + len(sub):]


# remise des lignes modifiées dans le fichier
with open(fileTarget, 'wt') as f:
    f.write(lines)
